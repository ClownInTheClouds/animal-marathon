package animal.marathon.entity;

import animal.marathon.entity.enumiration.ObstacleType;

public abstract class Obstacle {

    protected final ObstacleType type;
    protected final float distance;

    protected Obstacle(ObstacleType type, float distance) {
        this.type = type;
        this.distance = distance;
    }

    public ObstacleType getType() {
        return type;
    }

    public float getDistance() {
        return distance;
    }

    public abstract boolean tryPass(Animal animal);
}
