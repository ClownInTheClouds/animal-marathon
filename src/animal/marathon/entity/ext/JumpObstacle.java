package animal.marathon.entity.ext;

import animal.marathon.entity.Animal;
import animal.marathon.entity.Obstacle;
import animal.marathon.entity.enumiration.ObstacleType;

public class JumpObstacle extends Obstacle {

    public JumpObstacle(float distance) {
        super(ObstacleType.JUMP, distance);
    }

    @Override
    public boolean tryPass(Animal animal) {
        String nameString = animal.getType() + " " + animal.getName() + " can ";
        String eventName = String.format("jump max %.2fm. Tries to jump ", animal.getMaxJump());
        boolean jumpResult = animal.jump(getDistance());
        String eventResult = jumpResult ? "succeed" : "fails";
        System.out.println(nameString + eventName + getDistance() + "m and " + eventResult);
        return jumpResult;
    }
}
