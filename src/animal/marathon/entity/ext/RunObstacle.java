package animal.marathon.entity.ext;

import animal.marathon.entity.Animal;
import animal.marathon.entity.Obstacle;
import animal.marathon.entity.enumiration.ObstacleType;

public class RunObstacle extends Obstacle {

    public RunObstacle(float distance) {
        super(ObstacleType.RUN, distance);
    }

    @Override
    public boolean tryPass(Animal animal) {
        String nameString = animal.getType() + " " + animal.getName() + " can ";
        String eventName = String.format("run max %.2fm. Tries to run ", animal.getMaxRun());
        boolean runResult = animal.run(getDistance());
        String eventResult = runResult ? "succeed" : "fails";
        System.out.println(nameString + eventName + getDistance() + "m and " + eventResult);
        return runResult;
    }
}
