package animal.marathon.entity.ext;

import animal.marathon.entity.Animal;

public class Dog extends Animal {

    public Dog(String name, float maxJump, float maxRun, float maxSwim) {
        super("Dog", name, maxJump, maxRun, maxSwim);
    }

}
