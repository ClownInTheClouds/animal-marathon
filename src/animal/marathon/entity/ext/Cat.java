package animal.marathon.entity.ext;

import animal.marathon.entity.Animal;

public class Cat extends Animal {

    public Cat(String name, float maxJump, float maxRun, float maxSwim) {
        super("Cat", name, maxJump, maxRun, maxSwim);
    }

    @Override
    public int swim(float distance) {
        return Animal.SWIM_WTF;
    }
}