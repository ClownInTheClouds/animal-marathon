package animal.marathon.entity.ext;

import animal.marathon.entity.Animal;
import animal.marathon.entity.Obstacle;
import animal.marathon.entity.enumiration.ObstacleType;

public class SwimObstacle extends Obstacle {

    public SwimObstacle(float distance) {
        super(ObstacleType.SWIM, distance);
    }

    @Override
    public boolean tryPass(Animal animal) {
        String nameString = animal.getType() + " " + animal.getName() + " can ";
        int swimResult = animal.swim(getDistance());
        String eventName = String.format("swim max %.2fm. Tries to swim ", animal.getMaxSwim());
        String eventResult = (swimResult == Animal.SWIM_OK) ? "succeed" : "fails";
        if (swimResult == Animal.SWIM_WTF)
            eventResult = "too scared to enter the water";
        System.out.println(nameString + eventName + getDistance() + "m and " + eventResult);
        return swimResult == Animal.SWIM_OK;
    }
}
