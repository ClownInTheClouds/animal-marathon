package animal.marathon.entity;

import java.util.Random;

public abstract class Animal {
    public static final int SWIM_FAIL = 0;
    public static final int SWIM_OK = 1;
    public static final int SWIM_WTF = -1;

    private final String type;
    private final String name;
    private final float maxRun;
    private final float maxSwim;
    private final float maxJump;

    protected Animal(String type, String name, float maxJump, float maxRun, float maxSwim) {
        Random random = new Random();
        float jumpDiff = random.nextFloat() * 5 - 1;
        float runDiff = random.nextFloat() * 200 - 100;
        float swimDiff = random.nextFloat() * 5 - 2;

        this.type = type;
        this.name = name;
        this.maxJump = maxJump + jumpDiff;
        this.maxRun = maxRun + runDiff;
        this.maxSwim = maxSwim + swimDiff;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public float getMaxRun() {
        return this.maxRun;
    }

    public float getMaxSwim() {
        return this.maxSwim;
    }

    public float getMaxJump() {
        return this.maxJump;
    }

    public boolean run(float distance) {
        return (distance < maxRun);
    }

    public int swim(float distance) {
        return (distance < maxSwim) ? SWIM_OK : SWIM_FAIL;
    }

    public boolean jump(float height) {
        return (height < maxJump);
    }
}