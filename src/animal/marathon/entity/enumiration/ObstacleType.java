package animal.marathon.entity.enumiration;

public enum ObstacleType {
    RUN, JUMP, SWIM
}
