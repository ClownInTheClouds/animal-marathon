package animal.marathon.entity;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Marathon {

    private final List<Obstacle> obstacles;

    public Marathon(List<Obstacle> obstacles) {
        this.obstacles = obstacles;
    }

    public Map<Obstacle, Map<Animal, Boolean>> tryPass(List<Animal> animalTeam) {
        return obstacles.stream()
                .collect(Collectors.toMap(Function.identity(), obstacle -> animalTeam.stream()
                        .collect(Collectors.toMap(Function.identity(), obstacle::tryPass))));
    }
}
