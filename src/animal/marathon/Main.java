package animal.marathon;

import animal.marathon.entity.*;
import animal.marathon.entity.ext.*;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Obstacle sprintRun = new RunObstacle(100f);
        Obstacle marathonRun = new RunObstacle(10_000f);
        Obstacle poleVault = new JumpObstacle(1.5f);
        Obstacle swimInPool = new SwimObstacle(50f);

        Marathon marathon = new Marathon(List.of(sprintRun, marathonRun, poleVault, swimInPool));

        Cat barsikCat = new Cat("Barsik", 2f, 200f, 1f);
        Cat snowballCat = new Cat("Snowball", 0.3f, 10_000f, 1f);
        Dog tuzikDog = new Dog("Tuzik", 0.5f, 500f, 10f);
        Dog bobikDog = new Dog("Bobik", 1.5f, 10_000f, 50f);

        List<Animal> catTeam = List.of(barsikCat, snowballCat);
        List<Animal> dogTeam = List.of(tuzikDog, bobikDog);

        Map<Obstacle, Map<Animal, Boolean>> catTeamResults = marathon.tryPass(catTeam);
        System.out.println("catTeamResults = " + catTeamResults);
        Map<Obstacle, Map<Animal, Boolean>> dogTeamResults = marathon.tryPass(dogTeam);
        System.out.println("dogTeamResults = " + dogTeamResults);
    }
}